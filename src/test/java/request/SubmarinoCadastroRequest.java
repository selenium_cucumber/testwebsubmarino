package request;


import cucumber.api.java.en.Given;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class SubmarinoCadastroRequest {

    public static WebDriver navegador;

    @Given("que acessei o site")
    public void acessarSite(){

        System.setProperty("webdriver.chrome.driver", "src/driver/chromedriver.exe");
        navegador = new ChromeDriver();
        navegador.manage().window().maximize();
        navegador.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        navegador.get("https://cliente.submarino.com.br/simple-login/cadastro/pf");
    }


}
