package tests;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.SubmarinoCadastroRequest;

public class SubmarinoCadastroTest {

    SubmarinoCadastroRequest submarinoCadastroRequest = new SubmarinoCadastroRequest();
    public static WebDriver navegador;

   //Inserir Dados

   @When("inserir dados")
    public void inserirDados(){

       submarinoCadastroRequest.navegador.findElement(By.id("email-input")).sendKeys("Teste");
       submarinoCadastroRequest.navegador.findElement(By.name("password")).sendKeys("teste");
       submarinoCadastroRequest.navegador.findElement(By.id("cpf-input")).sendKeys("00000099988");
       submarinoCadastroRequest.navegador.findElement(By.id("name-input")).sendKeys("Teste nome");
       submarinoCadastroRequest.navegador.findElement(By.id("birthday-input")).sendKeys("10/10/2000");
       submarinoCadastroRequest.navegador.findElement(By.cssSelector("#gender > div:nth-child(3) > label")).click();
       submarinoCadastroRequest.navegador.findElement(By.id("phone-input")).sendKeys("51980999988");
       submarinoCadastroRequest.navegador.findElement(By.cssSelector("#root > div > div.cadastroForm-container > form > div:nth-child(8) > div > label > svg")).click();
   }

   @Then("valido a insercao")
    public void validoInsercao(){
       System.out.println("inserido");
       submarinoCadastroRequest.navegador.quit();
   }




}
