package tests;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import request.SubmarinoCadastroRequest;

public class SubmarinoCadastroExcessoesTest {
    SubmarinoCadastroRequest submarinoCadastroRequest = new SubmarinoCadastroRequest();
    public static WebDriver navegador;

    //não inserir email
    @When("nao inserir email")
    public void nao_inserir_email() throws Throwable {
        submarinoCadastroRequest.navegador.findElement(By.id("email-input")).sendKeys("");
        submarinoCadastroRequest.navegador.findElement(By.name("password")).click();
    }

    @Then("passou sem preencher email")
    public void valido_o_erro() throws Throwable {
        System.out.println("passou sem preencher email");
        submarinoCadastroRequest.navegador.quit();
    }

    //não inserir o CPF
    @When("nao inclui o CPF")
    public void nao_inclui_o_CPF() throws Throwable {
        submarinoCadastroRequest.navegador.findElement(By.id("email-input")).sendKeys("Teste");
        submarinoCadastroRequest.navegador.findElement(By.name("password")).sendKeys("teste");
       // submarinoCadastroRequest.navegador.findElement(By.id("cpf-input")).sendKeys("00000099988");
        submarinoCadastroRequest.navegador.findElement(By.id("name-input")).sendKeys("Teste nome");
    }

    @Then("passou sem preencher CPF")
    public void passou_sem_preencher_CPF() throws Throwable {
        System.out.println("passou sem preencher CPF");
        submarinoCadastroRequest.navegador.quit();
    }

    //inserir data de nascimento inválida e incompleta
    @When("inseri data de nascimento incompleta")
    public void inseri_data_de_nascimento_incompleta() throws Throwable {
        submarinoCadastroRequest.navegador.findElement(By.id("email-input")).sendKeys("Teste");
        submarinoCadastroRequest.navegador.findElement(By.name("password")).sendKeys("teste");
        submarinoCadastroRequest.navegador.findElement(By.id("cpf-input")).sendKeys("00000099988");
        submarinoCadastroRequest.navegador.findElement(By.id("name-input")).sendKeys("Teste nome");
        submarinoCadastroRequest.navegador.findElement(By.id("birthday-input")).sendKeys("15/15");
        submarinoCadastroRequest.navegador.findElement(By.cssSelector("#gender > div:nth-child(3) > label")).click();
   }

    @Then("passou sem validar formato da data de nascimento")
    public void passou_sem_validar_formato_da_data_de_nascimento() throws Throwable {
        System.out.println("passou com data de nascimento inválida");
        submarinoCadastroRequest.navegador.quit();
    }

}
