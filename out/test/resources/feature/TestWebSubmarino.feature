@Submarino
Feature: Validar fluxos de dados para incluir login no Submarino

  @InserirDados
  Scenario: Inserir Dados
    Given que acessei o site
    When inserir dados
    Then valido a insercao

  @NaoInserirEmail
  Scenario: Nao Inserir Email
    Given que acessei o site
    When  nao inserir email
    Then passou sem preencher email

  @NaoInserirCPF
  Scenario: Nao Inserir Email
    Given que acessei o site
    When  nao inclui o CPF
    Then passou sem preencher CPF

  @InserirDataNascimentoIncompleta
  Scenario: Inserir Data de Nascimento Incompleta
    Given que acessei o site
    When  inseri data de nascimento incompleta
    Then passou sem validar formato da data de nascimento